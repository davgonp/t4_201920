package test.data_structures;

import junit.framework.TestCase;
import model.data_structures.MaxColaPrioridad;
import model.data_structures.MaxHeapCP;
import model.logic.*;
import model.data_structures.*;

public class PruebaColaPrioridad extends TestCase
{

	MaxColaPrioridad <Integer> colaCP;
	MaxHeapCP <Integer> heapCP;
	MaxColaPrioridad<TravelTime> colaLocation;
	MaxHeapCP <TravelTime> heapLocation;
	public void setUp()
	{
		
		colaCP= new MaxColaPrioridad <Integer>();
		colaCP.agregar(4);
		colaCP.agregar(5);
		colaCP.agregar(3);
		heapCP= new MaxHeapCP <Integer>(6);
		heapCP.agregar(4);
		heapCP.agregar(2);
		heapCP.agregar(1);
	}
	public void setUp2()
	{
	    colaLocation = new MaxColaPrioridad<TravelTime>();
	    heapLocation = new MaxHeapCP<TravelTime>(10);
		for(int i = 0; i < 10; i++)
		{
			colaLocation.agregar(new TravelTime (i,i,i,i,i));
			heapLocation.agregar(new TravelTime(i,i,i,i,i));
		}
		
	}

	public void testAgregarCola()
	{
		colaCP.agregar(2);
		assertEquals("No se agreg� el elemento", 2, colaCP.darElemento(3).intValue());
	}
	public void testAgregarCola2()
	{
		setUp2();
		TravelTime dato = new TravelTime(11, 20, 11,20,20);
		colaLocation.agregar(dato);
		assertEquals("Error al cargar un location en al cola", dato.getMeanTravelTime(), colaLocation.darElemento(0).getMeanTravelTime());
	}

	public void testDelMaxCola()
	{
		colaCP.agregar(6);
		assertEquals("No se elimin� el elemento con mayor prioridad", 6, colaCP.delMax().intValue());
	}
	public void testDelMaxCola2()
	{
		setUp2();
		TravelTime dato = new TravelTime(11, 2, 11,1,1);
		colaLocation.agregar(dato);
		assertEquals("Error al cargar un location en al cola", dato.getMeanTravelTime(), colaLocation.delMax().getMeanTravelTime());
	}

	public void testSizeCola() throws Exception
	{
		assertEquals("El tama�o no es el esperado",3,colaCP.darNumElementos());
	}
	public void testSizeCola2()throws Exception
	{
		setUp2();
		assertEquals("El tama�o es incorrecto",10, colaLocation.darNumElementos());
	}


	public void testAgregarHeap()
	{
		heapCP.agregar(0);
		assertEquals("No se agreg� el elemento", 0, heapCP.darElemento(4).intValue());
	}
    public void testAgregarHeap2()
    {
    	setUp2();
    	TravelTime dato = new TravelTime(11, 2, 11,1,1);
		heapLocation.agregar(dato);
		assertEquals("Error al cargar un location en Heap", dato.getMeanTravelTime(), heapLocation.darElemento(1).getMeanTravelTime());
    }
	public void testDelMaxHeap()
	{
		heapCP.agregar(6);
		assertEquals("No se elimin� el elemento con mayor prioridad", 6, heapCP.delMax().intValue());
	}
	public void testDelMaxHeap2()
	{
		setUp2();
		TravelTime dato = new TravelTime(11, 2, 11,1,1);
		heapLocation.agregar(dato);
		assertEquals("Error al cargar un location en Heap", dato.getMeanTravelTime(), heapLocation.delMax().getMeanTravelTime());
	}

	public void testSizeQHeap() throws Exception
	{
		assertEquals("El tama�o no es el esperado",3,heapCP.darNumElementos());
	}
}