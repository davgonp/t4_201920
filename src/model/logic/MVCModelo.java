package model.logic;


import java.io.FileReader;
import java.io.IOException;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

import com.opencsv.CSVReader;


import model.data_structures.MaxColaPrioridad;
import model.data_structures.MaxHeapCP;




/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo 
{
	/**
	 * Atributos del modelo del mundo
	 */


	private MaxColaPrioridad<TravelTime> cola ; 

	private MaxHeapCP<TravelTime> heap ;

	private ArrayList<TravelTime> viajes;

	private ArrayList <TravelTime> retorno; 

	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo()
	{
		cola = new MaxColaPrioridad<TravelTime>();
		heap = new MaxHeapCP<TravelTime>(40000000);
		viajes = new ArrayList<TravelTime>();
		retorno = new ArrayList<TravelTime>();
	}






	public int cargar() {
		int rta =0;

		CSVReader reader = null;
		try 
		{

			reader = new CSVReader(new FileReader("./data/bogota-cadastral-2018-1-All-HourlyAggregate.csv"));
			String[] nextLine=reader.readNext();
			while ((nextLine = reader.readNext()) != null) 
			{
				TravelTime elem = new TravelTime(Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1]), 
						Integer.parseInt(nextLine[2]), Double.parseDouble(nextLine[3]),
						Double.parseDouble(nextLine[4]));

				viajes.add(elem);

				rta++;



			}



		} 

		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
		finally
		{
			if (reader != null) 
			{
				try 
				{
					reader.close();
				} catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}

		return rta;
	}
	
	
	public MaxColaPrioridad<TravelTime> darCola() {
		return cola;
	}
	
	public MaxHeapCP<TravelTime> darHeap() {
		return heap;
	}
	
	public ArrayList<TravelTime> darViajes() {
		return viajes;
	}

	public ArrayList<TravelTime> darMuestra(int seleccion)
	{
		while(retorno.size()<seleccion)
		{
			int j = ThreadLocalRandom.current().nextInt(0,viajes.size());
			retorno.add(viajes.get(j));
		}
		return retorno;
	}
	


	public long agregarDatosColaPrioridad(ArrayList<TravelTime> muestra)
	{
		long startTime = System.nanoTime();
		for(int i = 0; i < muestra.size(); i++)
		{
			cola.agregar(muestra.get(i)); 
		}
		long endTime = System.nanoTime() - startTime;
		long promedio = endTime/cola.darNumElementos();
		return promedio;
	}

	public long eliminarDatosColaPrioridad()
	{
		long startTime = System.nanoTime();
		int tamano=cola.darNumElementos();
		for(int i = 0; i < retorno.size(); i++)
		{
			cola.delMax();
		}
		long endTime = System.nanoTime() - startTime;
		long promedio = endTime/tamano;
		return promedio;
	}

	public long agregarDatosMaxHeap(ArrayList<TravelTime> muestra)
	{
		long startTime = System.nanoTime();
		for(int i = 0; i < muestra.size(); i++)
		{
			heap.agregar(muestra.get(i)); 
		}
		long endTime = System.nanoTime() - startTime;
		long promedio = endTime/ heap.darNumElementos();
		return promedio;
	}

	public long eliminarDatosMaxHeap()
	{
		long startTime = System.nanoTime();
		int tamano=heap.darNumElementos();
		for(int i = 0; i< retorno.size(); i++)
		{
			heap.delMax();
		}
		long endTime = System.nanoTime() - startTime;
		long promedio = endTime/tamano;
		return promedio;
	}
	

	public MaxColaPrioridad<TravelTime> crearMaxColaCP(int hInicial, int hFinal, int N)
	{
		ArrayList<TravelTime> data = new ArrayList<TravelTime>();
		for(int i = 0; i < viajes.size(); i++ )
		{
			TravelTime dato = viajes.get(i);


			if(dato.getMeanTravelTime()>=hInicial  && dato.getMeanTravelTime() <=hFinal)
			{
				data.add(dato);
			}
		}

		MaxColaPrioridad<TravelTime> retorno = new MaxColaPrioridad<TravelTime>();
		long timeCola = agregarDatosColaPrioridad(data);

		long startTime = System.nanoTime();
		for(int i = 0; i < N; i++)
		{
			TravelTime  respuesta =  cola.delMax();
			retorno.agregar(respuesta);
		}
		long endTime = (System.nanoTime() - startTime) + timeCola;

		System.out.println("El tiempo para cola de prioridad fue : " + endTime+ "\n" );

		return retorno;
	}
	

	public MaxHeapCP<TravelTime> crearMaxHeapCP(int hInicial, int hFinal, int N)
	{
		ArrayList<TravelTime> data = new ArrayList<TravelTime>();
		for(int i = 0; i < viajes.size(); i++ )
		{
			TravelTime dato = viajes.get(i);


			if(dato.getMeanTravelTime()>=hInicial  && dato.getMeanTravelTime() <=hFinal)
			{
				data.add(dato);
			}
		}

		MaxHeapCP<TravelTime> retorno = new MaxHeapCP<TravelTime>(1000000);

		long timeHeap = agregarDatosMaxHeap(data);

		long startTime = System.nanoTime();
		for(int i = 0; i < N; i++)
		{
			TravelTime  respuesta =  heap.delMax();
			retorno.agregar(respuesta);
		}
		long endTime = (System.nanoTime() - startTime)+ timeHeap;
		System.out.println("El tiempo para MaxHeap fue de :" + endTime);

		return retorno;
	}
}