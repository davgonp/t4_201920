package controller;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import model.data_structures.*;
import model.logic.MVCModelo;
import model.logic.TravelTime;
import model.logic.Viaje;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;

	/* Instancia de la Vista*/
	private MVCView view;

	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}

	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		Integer dato = null;
		Integer dato1 = null;
        Integer dato2 = null;
		Integer respuesta = null;
		

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
			case 1:
			
				System.out.println("Archivo cargado");
				System.out.println("Numero de elementos " + modelo.cargar() + "\n---------");	
				
				break;

			case 2:
				
				System.out.println("--------- \nDar tama�o a la muestra a ingresar: ");
		        dato2 = lector.nextInt();
		        System.out.println("--------- \n Ingrese la primera hora");
				dato = lector.nextInt();
				System.out.println("--------- \n Ingrese la segunda hora");
				dato1 = lector.nextInt();
		        ArrayList<TravelTime> muestra = modelo.darMuestra(dato2);
				System.out.println("Cola creada con tiempo promedio de: "+ modelo.agregarDatosColaPrioridad(muestra));
				System.out.println("Total de datos: "+ modelo.darCola().darNumElementos());
				System.out.println("Dato max: "+ modelo.darCola().max());
				System.out.println("Tiempo promedio del la maxima cola con respecto un intervalo de horas:");
                modelo.crearMaxColaCP(dato, dato1, dato2);				
				break;

			case 3:
				System.out.println("--------- \nDar tama�o a la muestra a ingresar: ");
		        dato2 = lector.nextInt();
		        System.out.println("--------- \n Ingrese la primera hora"); 
				dato = lector.nextInt();
				System.out.println("--------- \n Ingrese la segunda hora");
				dato1 = lector.nextInt();
		        ArrayList<TravelTime> muestra2 = modelo.darMuestra(dato2);
				System.out.println("Crear Heap de prioridad");
				System.out.println("Heap creado con tiempo promedio de: "+ modelo.agregarDatosMaxHeap(muestra2));
				System.out.println("Total de datos: "+ modelo.darHeap().darNumElementos());
				System.out.println("Dato max: "+ modelo.darHeap().max());
				System.out.println("Tiempo promedio del la maximo heap con respecto un intervalo de horas:");
                modelo.crearMaxHeapCP(dato, dato1, dato2);	
				break;


			case 4: 
				
	               			
				System.out.println("--------- \n Hasta pronto !! \n---------"); 
				lector.close();
				fin = true;
				break;
				

			default: lector.close();
				System.out.println("--------- \n Opcion Invalida !! \n---------");
				break;
			}
		}

	}

	
}
